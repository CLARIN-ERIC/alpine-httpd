= Alpine Linux with Apache httpd (`alpine-httpd`)
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

This project provides a Docker container image.
Using a container based on this image, you can serve a website using https://httpd.apache.org/[Apache httpd web server].

== Dependencies

[options="header",cols=",,,m"]
|===
| Conditions | Type | Name (URL) | Version constraint

| by necessity
| software
| https://www.docker.com/[Docker Compose]
| ==1.8.0

| by necessity
| software
| https://www.docker.com/[Docker Engine]
| ==1.11.2

| by necessity
| image
| https://github.com/gliderlabs/docker-alpine[`gliderlabs/alpine`]
| ==3.4

| for Docker Engine interaction
| software
| https://www.sudo.ws/[Sudo]
| >=1.8

| by necessity
| library
| https://gitlab.com/sander_maijers/sys-provisioning[`sys-provisioning`]
| ==0.3.1

| by necessity
| utility
| https://gitlab.com/sander_maijers/template-substitute[`template-substitute`]
| ==1.0.1

| for releases
| platform
| https://about.gitlab.[GitLab CI]
| ==8.10.4

|===

== To use

The best way to discover how to use the image is by reading the link:docker-compose.yml[`docker-compose.yml`] and link:deployment.yml[`deployment.yml`] files.
These specify how to deploy containers based on this image with Docker Compose.
link:test.sh[`test.sh`] contains information about the commands needed to deploy.

== To build and test

The build, test and release pipeline is specified in the the link:.gitlab-ci.yml[GitLab CI] config file.
This pipeline or its stages can be run by GitLab.com CI as well as by a self-hosted GitLab instance.
This is elaborated in the https://about.gitlab.com/gitlab-ci/[GitLab CI docs].
In addition, the build and test stages can be run be run locally within a bare Docker container, without a GitLab CI pipeline.

NOTE: Please issue all of the following shell statements from within the root directory of this repository.

=== Without a GitLab CI pipeline

[source,sh]
----
Docker_engine_ver="$(sudo docker version --format '{{.Server.Version}}')"
sudo docker run --volume='/var/run/docker.sock:/var/run/docker.sock' \
    --rm --volume="$PWD":"$PWD" --workdir="$PWD" -it \
    docker:"$Docker_engine_ver" sh
----

Within the container you now entered, to build and test without a time-out value:

[source,sh]
----
sh -x ./build.sh && sh -x ./test.sh
----

== Supported Parameters

_SERVERNAME     REQUIRED

== Certificate store

The SSL/TLC configuration requires a volume, either a docker volume or a host mount.
