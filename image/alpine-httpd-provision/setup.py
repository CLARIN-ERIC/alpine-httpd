# pylint: disable=missing-docstring
from setuptools import find_packages
from setuptools import setup

setup(
    author='Sander Maijers',
    classifiers=['Operating System :: POSIX',
                 'Intended Audience :: Developers'],
    description='Provisioning code for `alpine-httpd` Docker image',
    install_requires=['sys-provisioning==0.3.1'],
    license='CC0 1.0',
    name='alpine-httpd-provision',
    packages=find_packages(exclude=['tests*']), )
