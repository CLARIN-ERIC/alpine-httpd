#!/bin/sh

set -e
apk --quiet update --update-cache
apk --quiet add 'curl'
cd -- 'in/'
# shellcheck disable=SC2026
curl --fail --location --show-error --silent --remote-name --tlsv1.2 \
    --time-cond 'sys_provisioning-0.3.1-py3-none-any.whl' \
'https://gitlab.com/sander_maijers/sys-provisioning/builds/3094378/artifacts/'\
'file/dist/sys_provisioning-0.3.1-py3-none-any.whl'
