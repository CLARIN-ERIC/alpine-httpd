#!/bin/sh

set -e
apk --quiet update --update-cache
apk --quiet add 'curl'
cd -- 'in/'
# shellcheck disable=SC2026
curl --fail --location --show-error --silent --remote-name --tlsv1.2 \
    --time-cond 'template_substitute-1.0.1-py3-none-any.whl' \
'https://gitlab.com/sander_maijers/template-substitute/builds/3091690/'\
'artifacts/file/dist/template_substitute-1.0.1-py3-none-any.whl'
