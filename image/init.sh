#!/bin/bash
_INIT_FILE="${INIT_FILE:-/var/www/dynamic_cfg/initialised}"
while  [ ! -f  "${_INIT_FILE}" ]; do echo "Waiting for preparer (${_INIT_FILE}) to finish"; sleep 5; done